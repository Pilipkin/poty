<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Picture;
use App\Models\User;
use App\Http\Requests;

class UsersController extends Controller
{
    public function index()
    {
        return 'kek';
    }

    public function show($userName, $request)
    {
        $user = User::where('user_name', $userName)->with('pictures.tags')->firstOrFail();
        return $this->viewOrJSON($request, []);
    }
}
