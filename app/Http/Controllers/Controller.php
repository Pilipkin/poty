<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function viewOrJSON(Request $request, $data,  $viewName = 'index')
    {
        $data['global']['user'] = \Auth::user();
        if($request->method() == 'GET'){
            return view($viewName, ['context'=>$data]);
        }
        return json_encode($data);

    }
}
