<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Picture;
use App\Http\Requests\PictureRequest;
use Request;

class PicturesController extends Controller
{

    public function index(\Illuminate\Http\Request $request)
    {
        $pictures = Picture::all();

        return $this->viewOrJSON($request, ['gallery' => ['pictures' => $pictures]]);
    }

    public function show($id)
    {

        $picture = Picture::where('id', $id)->with('comments')->firstOrFail();
        //$picture = Picture::findOrFail($id);
        return view('pictures.show', compact('picture'));
    }

    public function create()
    {
        return view('pictures.create');
    }

    public function store(PictureRequest $request)
    {
        $input = $request->all();
        $input['user_id'] = $request->user()['id'];
        Picture::create($input);
        return redirect('pictures');
    }

    public function edit($id)
    {
        $picture = Picture::findOrFail($id);
        return  view('pictures.edit', compact('picture'));
    }

    public function update($id, PictureRequest $request)
    {
        $picture = Picture::findOrFail($id);
        $picture->update($request->all());

        return redirect('pictures');
    }

    public function addComment(Request $request)
    {
     dd($request);
        return 'qwe';
    }
}
