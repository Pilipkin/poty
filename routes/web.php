<?php


Route::get('/', function () {
    return view('welcome');
});

Route::get('pictures/create', ['middleware' => 'auth', 'uses' => 'PicturesController@create']);
Route::get('pictures/{id}', 'PicturesController@show');
Route::post('pictures', 'PicturesController@store');
Route::get('pictures/{id}/edit', 'PicturesController@edit');
Route::patch('pictures/{id}', 'PicturesController@update');

Route::match(['get', 'post'], 'user' , 'PicturesController@show');
Route::get('user/{name}', 'UsersController@show');

Route::match(['get', 'post'], 'gallery' , 'PicturesController@index');

Auth::routes();



Route::get('/home', 'HomeController@index');
