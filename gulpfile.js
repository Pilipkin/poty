const elixir = require('laravel-elixir');
var util = require('gulp-util');
var webpack = require('webpack');

require("laravel-elixir-webpack-advanced");

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

var userProfileWebpackConfig = require('./resources/assets/js/userProfile/webpack.config.js');
if (!!util.env.production) {
    userProfileWebpackConfig.plugins.push(
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': '"production"'
        })
    );
    userProfileWebpackConfig.plugins.push(
        new webpack.optimize.DedupePlugin()
    );
    userProfileWebpackConfig.plugins.push(
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false,
                drop_console: true,
                drop_debugger: true,
            },
            comments: false,
        })
    );
    userProfileWebpackConfig.plugins.push(
        new webpack.optimize.OccurrenceOrderPlugin(false)
    );
}

var nodeModulesJS = '../../../node_modules';
elixir(function (mix) {
    mix
        .copy('node_modules/font-awesome/fonts', 'public/fonts')
        .copy('node_modules/bootstrap/dist/fonts', 'public/fonts')
        .scripts([
            nodeModulesJS + '/jquery/dist/jquery.min.js',
            nodeModulesJS + '/bootstrap/dist/js/bootstrap.min.js',
            'ajaxConfig.js'
        ], 'public/js/vendor.js')
        .sass('app.scss')
        .webpack(
            { userProfile: 'userProfile/index.js'},
            userProfileWebpackConfig
        )
        .browserSync({
            proxy: 'poty'
        })
    ;
});