@extends('layouts.app')
@section('content')
        <!DOCTYPE html>
<html lang="en">
<!--
<link rel="stylesheet" href="{{asset('css/userProfile.css?').rand()}}">
<link rel="stylesheet" href="{{asset('css/vendors/react-toggle.css?').rand()}}">
-->
<script>
    window.user = {!! $user->toJson() !!}
</script>
<body>
<div id="app-container"></div>
</body>

<script src="https://code.jquery.com/jquery-3.1.0.min.js"></script>
<script src="{{asset('js/userProfile/vendor.js?').rand()}}"></script>
<script src="{{asset('js/userProfile/userProfile.js?').rand()}}"></script>

</html>
@endsection