@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="show-picture" style="text-align:center">
            <h1>{{ $picture->title }}</h1>
            <img src="{{ $picture->url }}" alt="url ustarelo" style="width: 100%;" />
            <p>
                {{ $picture->description }}
            </p>

            <div class="row">
                <ul class="list-group">
                    @foreach($picture->comments as $comment)
                    <li class="list-group-item">
                        <span class="tag tag-default tag-pill float-xs-right"></span>
                        {{ $comment->body }}
                    </li>
                    @endforeach
                </ul>
                {!! Form::model(['action' => ['PicturesController@addComment']]) !!}
                <div class="form-group" style="width:50%">
                    {!! Form::hidden('user_id',Auth::user()->id) !!}
                    {!! Form::hidden('picture_id',$picture->id) !!}
                    {!! Form::label('title', 'Добавить комментарий') !!}
                    {!! Form::text('title' , null, ['class' => 'form-control']) !!}
                </div>
                {!! Form::close() !!}
            </div>

        </div>
    </div>

@endsection
