<div class="form-group" style="width:50%">
    {!! Form::hidden('user_id',1) !!}
    {!! Form::label('title', 'Название') !!}
    {!! Form::text('title' , null, ['class' => 'form-control']) !!}
</div>
<div class="form-group" style="width:50%">
    {!! Form::label('description', 'Описание') !!}
    {!! Form::textarea('description' , null, ['class' => 'form-control']) !!}
</div>
<div class="form-group" style="width:50%">
    {!! Form::label('url', 'Url') !!}
    {!! Form::text('url' , null, ['class' => 'form-control']) !!}
</div>
<div class="form-group" style="width:50%">
    {!! Form::submit($submitButtonText, ['class' => 'btn btn-primary form-control']) !!}
</div>
