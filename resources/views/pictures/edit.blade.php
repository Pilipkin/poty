@extends('layouts.app')
@section('content')
    <h1>Изменить : {{ $picture->title }}</h1>
    {!! Form::model($picture, ['method' => 'PATCH', 'action' => ['PicturesController@update', $picture->id]]) !!}
    @include('pictures._form', ['submitButtonText' => 'Изменить картинку'])
    {!! Form::close() !!}
    @include('errors.list')
@endsection
