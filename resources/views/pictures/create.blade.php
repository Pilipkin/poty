@extends('layouts.app')
@section('content')
    <div class="">
    {!! Form::open(['url' => 'pictures']) !!}
    @include('pictures._form', ['submitButtonText' => 'Добавить картинку'])
    {!! Form::close() !!}
    </div>
    @include('errors.list')
@endsection
