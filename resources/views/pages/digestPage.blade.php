@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row" style="text-align: center;">
            <h1>Картинка дня</h1>

            <img style="width:80%" src="https://yandex.ru/images/today" alt="" />
            <hr>
            <h1 style="text-align:center">Популярные за сегодня</h1>
        </div>
        <hr>
        <div class="row">
            @foreach ($pictures as $picture)
                <div >
                    <div class="card" style="display: block; float: left; width: 33.333%; padding: 0.75rem; margin-bottom: 2rem; border:0; background-color: #ffffff">
                      <img data-src="holder.js/100px280/thumb" alt="100%x280" class="img-thumbnail" src="{{ $picture->url }}" >
                        <p class="card-text">{{ $picture->description }}</p>
                        <a href="{{ url('/pictures', $picture->id) }}" class="card-link">Подробности</a>
                        <a href="{{ url('/pictures', [$picture->id, 'edit'])}}" class="card-link">Изменить</a>

                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
