@extends('layouts.app')
@section('content')
    <script>
        window.context = {!! json_encode($context) !!}
    </script>
    <div id="app-container"></div>
    <script src="https://code.jquery.com/jquery-3.1.0.min.js"></script>
    <script src="{{asset('js/userProfile/vendor.js?').rand()}}"></script>
    <script src="{{asset('js/userProfile/userProfile.js?').rand()}}"></script>


@endsection
