@if ($errors->any())
    <div class="alert alert-warning" style="width:50%">
         @foreach ($errors->all() as $error)
             <span >{{ $error }}</span> <hr>
         @endforeach
     </div>
@endif
