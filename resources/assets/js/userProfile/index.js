import React from 'react'
import ReactDOM from 'react-dom'
import { Provider, connect} from 'react-redux'
import { Router, Route, browserHistory } from 'react-router'
import { syncHistoryWithStore } from 'react-router-redux'
import {configureStore} from './Reducers/Root'
import {mapStateToProps, mapDispatchToProps } from './Utils/ReduxMappers';
import Gallery from "./Containers/Gallery.jsx";
import UserPage from "./Containers/UserPage.jsx";

const store = configureStore();
const connector = connect(mapStateToProps, mapDispatchToProps);

ReactDOM.render(
    <Provider store={store}>
        <Router history={syncHistoryWithStore(browserHistory, store)}>
            <Route path="/gallery" component={connector(Gallery)}/>
            <Route path="/user" component={connector(UserPage)}/>
        </Router>
    </Provider>,
    document.getElementById('app-container')
);