export const actions = {
    GLOBAL_APP_LOAD_START : 'GLOBAL_APP_LOAD_START',
    GLOBAL_APP_LOAD_END : 'GLOBAL_APP_LOAD_END'
};

export var globalActions = {
    loadStart: function(){
      return {
          type: actions.GLOBAL_APP_LOAD_START
      }
    },
    loadEnd: function(){
      return {
          type: actions.GLOBAL_APP_LOAD_END
      }
    }
};