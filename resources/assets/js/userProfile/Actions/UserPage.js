export const actions = {
    USER_PAGE_TOGGLE_TAB : 'USER_PAGE_TOGGLE_TAB',

};

export var userPageActions = {
    toggleTab: function(id){
        return {
            type: actions.USER_PAGE_TOGGLE_TAB,
            id : id
        }
    },

};