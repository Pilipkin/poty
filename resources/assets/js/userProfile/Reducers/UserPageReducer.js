import initialState from '../Utils/InitialState';
import {actions} from '../Actions/UserPage';

export default function userPageReducer(state = initialState.userPage, action){
    switch(action.type){
        case actions.USER_PAGE_TOGGLE_TAB :
            return Object.assign({}, state, {
                currentTab : action.id
            });
        default :
            return state
    }
}
