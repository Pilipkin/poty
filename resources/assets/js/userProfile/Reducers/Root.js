import {combineReducers, createStore, compose, applyMiddleware} from 'redux';
import { browserHistory } from 'react-router'
import { routerMiddleware } from 'react-router-redux'
import {routerReducer} from 'react-router-redux';
import initialState from '../Utils/InitialState';
import globalReducer from './GlobalReducer';
import userPageReducer from './UserPageReducer';
import galleryReducer from './GalleryReducer';
var reduxThunk = require('redux-thunk');

export var rootReducer = combineReducers({
    global: globalReducer,
    userPage: userPageReducer,
    gallery: galleryReducer,
    routing: routerReducer
});

export function configureStore(state = initialState){
    return createStore(rootReducer, state, compose(
        applyMiddleware(reduxThunk.default, routerMiddleware(browserHistory)),
        window.devToolsExtension ? window.devToolsExtension() : f => f
    ));
}

