import initialState from '../Utils/InitialState';
import {actions} from '../Actions/Global';

export default function globalReducer(state = initialState.global, action){
    switch(action.type){
        case actions.GLOBAL_APP_LOAD_START :
            return Object.assign({}, state, {
                loading : true
            });
        case actions.GLOBAL_APP_LOAD_END :
            return Object.assign({}, state, {
                loading : false
            });
        default :
            return state
    }
}
