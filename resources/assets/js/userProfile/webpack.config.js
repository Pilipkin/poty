var path = require('path');
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var helpers = require('./helpers');
var webpack = require('webpack');

module.exports = {
    entry: "./index.js",
    output: {
        path: path.join(__dirname, './../../../../public/js/userProfile/'),
        publicPath: `//assets//js//userProfile` // Browser output path: /assets/js/
    },
    module: {
        loaders: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                loader: 'babel',
                query: { presets: [ 'es2015', 'react', 'stage-0' ] }
            },{
                test: /\.css$/,
                loader: "style-loader!css-loader",
            }
        ]
    },
    //plugins: [
    //    new ExtractTextPlugin("[name].css"),
    //],
    plugins: [
        new ExtractTextPlugin("[name].css"),
        /*new webpack.DefinePlugin({
            'process.env.NODE_ENV': '"production"'
        }),
        new webpack.optimize.DedupePlugin(),
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false
            }
        })*/
    ],
    resolve: {
        root: path.resolve('./'),
        extensions: ['', '.js', '.jsx', '.css']
    },
    //devtool: 'eval',
    devtool: 'source-map'
};