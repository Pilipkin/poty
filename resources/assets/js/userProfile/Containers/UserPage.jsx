import React from 'react';

export default class UserPage extends React.Component {
    constructor(props) {
        super(props);
        console.log(this.props);
    }
    componentWillReceiveProps(){

    };
    clickToHello(){
        if(this.props.global.loading){
            this.props.actions.global.loadEnd()
        } else {
            this.props.actions.global.loadStart()
        }
        this.props.router.goBack();
    }
    render(){
        return (
            <div className="container">
                <div className="user-left"></div>
                <button className="btn btn-success"
                        onClick={this.clickToHello.bind(this)}>
                    Hello,  asd
                </button>
                {
                    this.props.global.loading?
                        <p>loading...</p>:
                        <p>Not Loading!</p>
                }
            </div>
        )
    }
}
