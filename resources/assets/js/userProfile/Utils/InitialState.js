var  initialState = {
    global:{
        loading : false,
        user : null
    },
    userPage:{
        currentTab: 'default',
    },
    gallery: {
        pictures: []
    }
};

function applyContext(context, state){
    for(let i in context){
        let ss = context[i];
        for(let j in ss){
            state[i][j] = ss[j];
        }
    }
}

applyContext(window.context, initialState);

export default  initialState
