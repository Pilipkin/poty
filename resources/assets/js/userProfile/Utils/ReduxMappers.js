import {Redux, bindActionCreators} from 'redux';
import {globalActions} from '../Actions/Global';
import {userPageActions} from '../Actions/UserPage';
import {galleryActions} from '../Actions/Gallery';

export function mapStateToProps(state){
    return {
        global : state.global,
        userPage : state.userPage,
        gallery : state.gallery
    }
}

export function mapDispatchToProps(dispatch){
    return {
        actions: {
            global: bindActionCreators(globalActions, dispatch),
            userPage: bindActionCreators(userPageActions, dispatch),
            gallery: bindActionCreators(galleryActions, dispatch)
        }
    }
}