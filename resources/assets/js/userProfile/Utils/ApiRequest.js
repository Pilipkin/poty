export default class ApiRequest{

    static Get( method, payload, options ){

        return this._process(
            "GET", method, payload, options
        );
    }

    static Post( method, payload, options ){

        return this._process(
            "POST", method, payload, options
        );
    }

    static _process( type, method, payload, options ){
        return new Promise((resolve, reject)=>{

            options = options || {};

            if ( options.beforeSend ){
                options.beforeSend = ()=>{
                    options.beforeSend();
                }
            }

            let requestData = options;

            requestData.type = type;
            requestData.url
                = this.apiUrl + method + "?" + $.param(ApiRequest.attachedQuery);
            requestData.data = payload;

            $.ajax(requestData)
                .then((r)=>{
                    resolve(r);
                }).fail((e)=>{
                    reject(e);
                })
        });
    }
}

ApiRequest.appUrl = "http://localhost:3000";
ApiRequest.apiUrl = ApiRequest.appUrl + "";
ApiRequest.attachedQuery = {};

